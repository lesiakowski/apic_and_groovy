FROM groovy:latest
USER root
RUN mkdir -p /var/lib/apt/lists/partial
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash - \
    && apt-get update \
    && apt-get install -y nodejs \
    && npm install -g apiconnect@3.0.17
USER groovy
RUN apic --accept-license --disable-analytics